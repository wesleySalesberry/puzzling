const { Engine, Render, Runner, World, Bodies, Body, Events } = Matter;

// const width = 600;
// const height = 600;
const width = window.innerWidth - 10;
const height = window.innerHeight - 10;
//Grids
const cellsVertical = 10;
const cellsHorizontal = 15;

//every cell created is this units tall and this unit wide
const unitLengthX = width / cellsHorizontal;
const unitLengthY = height / cellsVertical;

const engine = Engine.create();
engine.world.gravity.y = 0;

const { world } = engine;
const render = Render.create({
	element: document.body,
	engine: engine,
	options: {
		wireframes: false,
		width,
		height,
		render: {
			fillStyle: 'black'
		}
	}
});

Render.run(render);
Runner.run(Runner.create(), engine);

//walls
const walls = [
	//Top wall
	Bodies.rectangle(width / 2, 0, width, 2, {
		isStatic: true,
		render: {
			fillStyle: 'yellow'
		}
	}),
	//Bottom Wall
	Bodies.rectangle(width / 2, height, width, 2, {
		isStatic: true,
		render: {
			fillStyle: 'yellow'
		}
	}),
	//left
	Bodies.rectangle(0, height / 2, 2, height, {
		isStatic: true,
		render: {
			fillStyle: 'yellow'
		}
	}),
	//Right
	Bodies.rectangle(width, height / 2, 2, height, {
		isStatic: true,
		render: {
			fillStyle: 'yellow'
		}
	})
];

World.add(world, walls);

//Maze generation
const shuffle = (arr) => {
	let counter = arr.length;

	while (counter > 0) {
		const index = Math.floor(Math.random() * counter);
		counter--;

		const temp = arr[counter];
		arr[counter] = arr[index];
		arr[index] = temp;
	}
};
//The first array adds rows the second adds columns
const grid = Array(cellsVertical).fill(null).map(() => Array(cellsHorizontal).fill(false));
const verticals = Array(cellsVertical).fill(null).map(() => Array(cellsHorizontal - 1).fill(false));
const horizontals = Array(cellsVertical - 1).fill(null).map(() => Array(cellsHorizontal).fill(false));
//console.log(grid);

const startRow = Math.floor(Math.random() * cellsVertical);
const startColumn = Math.floor(Math.random() * cellsHorizontal);

//console.log(startRow, startColumns);

const stepThroughCell = (row, column) => {
	//If i have visted the cell at [row, column]
	if (grid[row][column]) {
		return;
	}
	//Mark cell as being visited
	grid[row][column] = true;
	//assemble randomly-ordeered list of neighbors
	const neighbors = [
		[ row - 1, column, 'up' ],
		[ row, column + 1, 'right' ],
		[ row + 1, column, 'down' ],
		[ row, column - 1, 'left' ]
	];
	shuffle(neighbors);

	//for each neighbor
	for (let neighbor of neighbors) {
		const [ nextRow, nextColumn, direction ] = neighbor;
		//check to see that neighbor is out of bounds
		if (nextRow < 0 || nextRow >= cellsVertical || nextColumn < 0 || nextColumn >= cellsHorizontal) {
			continue;
		}
		//see if we visted that neighbor, continue to next neighbor
		if (grid[nextRow][nextColumn]) {
			continue;
		}
		//remove a wall from either horizontal or vertical
		if (direction === 'left') {
			verticals[row][column - 1] = true;
		} else if (direction === 'right') {
			verticals[row][column] = true;
		} else if (direction === 'up') {
			horizontals[row - 1][column] = true;
		} else if (direction === 'down') {
			horizontals[row][column] = true;
		}
		stepThroughCell(nextRow, nextColumn);
	}
	// Visit the next cell
};
stepThroughCell(startRow, startColumn);
//console.log(grid);

horizontals.forEach((row, rowIndex) => {
	row.forEach((open, columnIndex) => {
		if (open === true) {
			return;
		}

		const wall = Bodies.rectangle(
			columnIndex * unitLengthX + unitLengthX / 2,
			rowIndex * unitLengthY + unitLengthY,
			unitLengthX,
			3,
			{
				label: 'wall',
				isStatic: true,
				render: {}
			}
		);
		World.add(world, wall);
	});
});

verticals.forEach((row, rowIndex) => {
	row.forEach((open, columnIndex) => {
		if (open) {
			return;
		}

		const wall = Bodies.rectangle(
			columnIndex * unitLengthX + unitLengthX,
			rowIndex * unitLengthY + unitLengthY / 2,
			5,
			unitLengthY,
			{
				label: 'wall',
				isStatic: true,
				render: {
					fillStyle: 'green'
				}
			}
		);
		World.add(world, wall);
	});
});
//Goal
const goal = Bodies.rectangle(width - unitLengthX / 2, height - unitLengthY / 2, unitLengthX * 0.7, unitLengthY * 0.7, {
	isStatic: true,
	label: 'goal',
	render: {
		fillStyle: 'green'
	}
});
World.add(world, goal);

//Ball
const ballRadius = Math.min(unitLengthX, unitLengthY) / 4;
const ball = Bodies.circle(unitLengthX / 2, unitLengthY / 2, ballRadius, {
	label: 'ball',
	render: {
		fillStyle: 'orange'
	}
});

World.add(world, ball);

document.addEventListener('keydown', (event) => {
	const { x, y } = ball.velocity;
	console.log(x, y);
	if (event.keyCode === 87 || event.keyCode === 38) {
		console.log('up');
		Body.setVelocity(ball, { x, y: y - 5 });
	}
	if (event.keyCode === 83 || event.keyCode === 40) {
		console.log('down');
		Body.setVelocity(ball, { x, y: y + 5 });
	}
	if (event.keyCode === 65 || event.keyCode === 37) {
		console.log('left');
		Body.setVelocity(ball, { x: x - 5, y });
	}
	if (event.keyCode === 68 || event.keyCode === 39) {
		console.log('right');
		Body.setVelocity(ball, { x: x + 5, y });
	}
});

//Win condition
Events.on(engine, 'collisionStart', (event) => {
	event.pairs.forEach((collision) => {
		const labels = [ 'ball', 'goal' ];
		if (labels.includes(collision.bodyA.label) && labels.includes(collision.bodyB.label)) {
			document.querySelector('.winner').classList.remove('hidden');
			world.gravity.y = 1;
			world.bodies.forEach((body) => {
				if (body.label === 'wall') {
					Body.setStatic(body, false);
					// setTimeout(() => {

					// })
				}
			});
		}
	});
});
